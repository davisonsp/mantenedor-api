package cl.coopeuch.challenge.mantenedorapi.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import cl.coopeuch.challenge.mantenedorapi.bo.TaskBo;
import cl.coopeuch.challenge.mantenedorapi.dto.request.ReqEditTaskDto;
import cl.coopeuch.challenge.mantenedorapi.dto.request.ReqRegisterTaskDto;

public class TaskControllerTest {

  @InjectMocks
  private TaskController taskController;

  @Mock
  TaskBo taskBo;

  @Before
  public void initMocks() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void test1() {
    ResponseEntity<Object> response = new ResponseEntity<>("", HttpStatus.OK);
    Mockito.when(taskBo.getAllTask()).thenReturn(response);
    taskController.getAllTask();
  }

  @Test
  public void test2() {
    ResponseEntity<Object> response = new ResponseEntity<>("", HttpStatus.OK);
    Mockito.when(taskBo.getTask(Mockito.anyLong())).thenReturn(response);
    taskController.getTask(1L);
  }

  @Test
  public void test3() {
    ResponseEntity<Object> response = new ResponseEntity<>("", HttpStatus.OK);
    Mockito.when(taskBo.registerTask(Mockito.mock(ReqRegisterTaskDto.class))).thenReturn(response);
    taskController.registerTask(new ReqRegisterTaskDto());
  }

  @Test
  public void test4() {
    ReqEditTaskDto req = new ReqEditTaskDto();
    ResponseEntity<Object> response = new ResponseEntity<>("", HttpStatus.OK);
    Mockito.when(taskBo.editTask(Mockito.anyLong(), Mockito.any())).thenReturn(response);
    taskController.editTask(1L, req);
  }

  @Test
  public void test5() {
    ResponseEntity<Object> response = new ResponseEntity<>("", HttpStatus.OK);
    Mockito.when(taskBo.removeTask(Mockito.anyLong())).thenReturn(response);
    taskController.removeTask(1L);
  }

}
