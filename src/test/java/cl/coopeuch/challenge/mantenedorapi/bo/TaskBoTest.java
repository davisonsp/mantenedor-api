package cl.coopeuch.challenge.mantenedorapi.bo;

import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import cl.coopeuch.challenge.mantenedorapi.dto.request.ReqEditTaskDto;
import cl.coopeuch.challenge.mantenedorapi.dto.request.ReqRegisterTaskDto;
import cl.coopeuch.challenge.mantenedorapi.repository.model.Task;
import cl.coopeuch.challenge.mantenedorapi.service.ITaskService;

public class TaskBoTest {

  @InjectMocks
  @Spy
  private TaskBo taskBo;

  @Mock
  private ITaskService taskService;

  @Before
  public void initMocks() {
    MockitoAnnotations.openMocks(this);
  }

  @Test
  public void test1() {
    List<Task> listTask = new ArrayList<>();
    listTask.add(new Task());

    Mockito.when(taskService.getAllTask()).thenReturn(listTask);
    taskBo.getAllTask();
  }

  @Test
  public void test2() {
    List<Task> listTask = new ArrayList<>();

    Mockito.when(taskService.getAllTask()).thenReturn(listTask);
    taskBo.getAllTask();
  }

  @Test
  public void test3() {
    Task task = new Task();
    Mockito.when(taskService.getTask(Mockito.anyLong())).thenReturn(task);
    taskBo.getTask(1L);
  }

  @Test
  public void test4() {
    Task task = null;
    Mockito.when(taskService.getTask(Mockito.anyLong())).thenReturn(task);
    taskBo.getTask(1L);
  }

  @Test
  public void test5() {
    Task task = new Task();
    Mockito.when(taskService.saveTask(task)).thenReturn(task);
    taskBo.registerTask(new ReqRegisterTaskDto("description"));
  }

  @Test
  public void test6() {
    Task task = new Task();
    Mockito.when(taskService.getTask(Mockito.anyLong())).thenReturn(task);
    Mockito.when(taskService.saveTask(task)).thenReturn(task);
    taskBo.editTask(1L, new ReqEditTaskDto("description", '1'));
  }

  @Test
  public void test7() {
    Mockito.when(taskService.getTask(Mockito.anyLong())).thenReturn(null);
    taskBo.editTask(1L, new ReqEditTaskDto("description", '1'));
  }

  @Test
  public void test8() {
    Task task = new Task();
    Mockito.when(taskService.getTask(Mockito.anyLong())).thenReturn(task);
    Mockito.when(taskService.saveTask(task)).thenReturn(task);
    taskBo.removeTask(1L);
  }

  @Test
  public void test9() {
    Mockito.when(taskService.getTask(Mockito.anyLong())).thenReturn(null);
    taskBo.removeTask(1L);
  }

}
