package cl.coopeuch.challenge.mantenedorapi.repository.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public class BaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  @Column(name = "created_date")
  @Temporal(TemporalType.TIMESTAMP)
  protected Date createdDate;

  public BaseEntity() {
    this.createdDate = new Date();
  }

  public BaseEntity(Date createdDate) {
    this.createdDate = createdDate;
  }

  @PrePersist
  public void prePersist() {
    this.createdDate = new Date();
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

}
