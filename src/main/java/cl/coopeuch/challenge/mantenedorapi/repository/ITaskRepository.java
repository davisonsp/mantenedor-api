package cl.coopeuch.challenge.mantenedorapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import cl.coopeuch.challenge.mantenedorapi.repository.model.Task;

public interface ITaskRepository extends JpaRepository<Task, Long> {
}
