package cl.coopeuch.challenge.mantenedorapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MantenedorApplication {

  public static void main(String[] args) {
    SpringApplication.run(MantenedorApplication.class, args);
  }
}
