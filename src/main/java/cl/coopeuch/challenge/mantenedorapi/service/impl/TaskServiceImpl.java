package cl.coopeuch.challenge.mantenedorapi.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cl.coopeuch.challenge.mantenedorapi.repository.ITaskRepository;
import cl.coopeuch.challenge.mantenedorapi.repository.model.Task;
import cl.coopeuch.challenge.mantenedorapi.service.ITaskService;

@Service
public class TaskServiceImpl implements ITaskService {

  @Autowired
  private ITaskRepository taskRepository;

  @Override
  public List<Task> getAllTask() {
    return taskRepository.findAll();
  }

  @Override
  public Task getTask(Long id) {
    return taskRepository.findById(id).orElse(null);
  }

  @Override
  public Task saveTask(Task task) {
    return taskRepository.save(task);
  }

}
