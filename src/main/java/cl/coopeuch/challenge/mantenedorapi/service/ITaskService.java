package cl.coopeuch.challenge.mantenedorapi.service;

import java.util.List;
import cl.coopeuch.challenge.mantenedorapi.repository.model.Task;

public interface ITaskService {

  List<Task> getAllTask();

  Task getTask(Long id);

  Task saveTask(Task task);

}
