package cl.coopeuch.challenge.mantenedorapi.exception;

import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import cl.coopeuch.challenge.mantenedorapi.dto.Res;
import cl.coopeuch.challenge.mantenedorapi.enums.EFrontendMessage;

@ControllerAdvice
public class MantenedorAdvice extends ResponseEntityExceptionHandler {

  private static final Logger LOGGER_ = LogManager.getLogger(MantenedorAdvice.class);

  @ExceptionHandler(Exception.class)
  ResponseEntity<Object> handleControllerException(Exception e) {
    e.printStackTrace();
    LOGGER_.warn("MftApiAdvice - Exception : {}", e.getMessage());
    return new ResponseEntity<>(new Res(EFrontendMessage.ERROR_REST_SERVICE),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    Map<String, String> errors = new HashMap<>();
    ex.getBindingResult().getAllErrors()
        .forEach(error -> errors.put(((FieldError) error).getField(), error.getDefaultMessage()));
    return new ResponseEntity<>(new Res(EFrontendMessage.ERROR_REST_SERVICE, errors), status);
  }

}
