package cl.coopeuch.challenge.mantenedorapi.dto.request;

import javax.validation.constraints.NotEmpty;

public class ReqRegisterTaskDto {

  @NotEmpty(message = "descripcion no debe ser vacio")
  private String description;

  public ReqRegisterTaskDto() {}

  public ReqRegisterTaskDto(String description) {
    this.description = description;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

}
