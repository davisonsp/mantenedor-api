package cl.coopeuch.challenge.mantenedorapi.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class ReqEditTaskDto {

  @NotEmpty(message = "descripcion no debe ser vacio")
  private String description;

  @NotNull(message = "vigencia no debe ser vacio")
  private Character active;

  public ReqEditTaskDto() {}

  public ReqEditTaskDto(String description, Character active) {
    this.description = description;
    this.active = active;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Character getActive() {
    return active;
  }

  public void setActive(Character active) {
    this.active = active;
  }

}
