package cl.coopeuch.challenge.mantenedorapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import cl.coopeuch.challenge.mantenedorapi.enums.EFrontendMessage;

@JsonInclude(Include.NON_NULL)
public class Res {

  private String code;
  private String title;
  private String description;
  private Object data;

  public Res(EFrontendMessage eFrontendMessage) {
    this.code = eFrontendMessage.getCode();
    this.title = eFrontendMessage.getTitle();
    this.description = eFrontendMessage.getDescription();
  }

  public Res(EFrontendMessage eFrontendMessage, Object data) {
    this.code = eFrontendMessage.getCode();
    this.title = eFrontendMessage.getTitle();
    this.description = eFrontendMessage.getDescription();
    this.data = data;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

}
