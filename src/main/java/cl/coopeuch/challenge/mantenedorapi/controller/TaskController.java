package cl.coopeuch.challenge.mantenedorapi.controller;

import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cl.coopeuch.challenge.mantenedorapi.bo.TaskBo;
import cl.coopeuch.challenge.mantenedorapi.dto.request.ReqEditTaskDto;
import cl.coopeuch.challenge.mantenedorapi.dto.request.ReqRegisterTaskDto;
import cl.coopeuch.challenge.mantenedorapi.repository.model.Task;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RestController
@RequestMapping(value = "/task")
@CrossOrigin
public class TaskController {

  private static final Logger LOGGER = LogManager.getLogger(TaskController.class);

  @Autowired
  private TaskBo taskBo;

  @Operation(summary = "Obtener tarea")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Tarea encontrada",
          content = {@Content(mediaType = "application/json",
              schema = @Schema(implementation = Task.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid id supplied", content = @Content),
      @ApiResponse(responseCode = "404", description = "Book not found", content = @Content)})
  @GetMapping(value = "/get-all", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getAllTask() {
    LOGGER.info("TaskController - getAllTask()");
    return taskBo.getAllTask();
  }

  @Operation(summary = "Obtener todas las tareas")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Tareas encontradas",
          content = {@Content(mediaType = "application/json",
              schema = @Schema(implementation = Task.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid", content = @Content)})
  @GetMapping(value = "/get/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> getTask(@PathVariable("id") Long id) {
    LOGGER.info("TaskController - getUser()");
    return taskBo.getTask(id);
  }

  @Operation(summary = "Guardar tarea")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Tarea guardada",
          content = {@Content(mediaType = "application/json",
              schema = @Schema(implementation = Task.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid request supplied",
          content = @Content)})
  @PostMapping(value = "/register-task", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> registerTask(
      @Valid @RequestBody ReqRegisterTaskDto reqRegisterTraineeDto) {
    LOGGER.info("TaskController - registerTrainee()");
    return taskBo.registerTask(reqRegisterTraineeDto);
  }

  @Operation(summary = "Editar tarea")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Tarea editada",
          content = {@Content(mediaType = "application/json",
              schema = @Schema(implementation = Task.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid request supplied",
          content = @Content)})
  @PutMapping(value = "/edit-task/{id}", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> editTask(@PathVariable("id") Long id,
      @Valid @RequestBody ReqEditTaskDto reqEditTaskDto) {
    LOGGER.info("TaskController - editTask()");
    return taskBo.editTask(id, reqEditTaskDto);
  }

  @Operation(summary = "Eliminar tarea")
  @ApiResponses(value = {
      @ApiResponse(responseCode = "200", description = "Tarea eliminada",
          content = {@Content(mediaType = "application/json",
              schema = @Schema(implementation = Task.class))}),
      @ApiResponse(responseCode = "400", description = "Invalid", content = @Content)})
  @DeleteMapping(value = "/remove-task/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Object> removeTask(@PathVariable("id") Long id) {
    LOGGER.info("TaskController - removeTask()");
    return taskBo.removeTask(id);
  }

}
