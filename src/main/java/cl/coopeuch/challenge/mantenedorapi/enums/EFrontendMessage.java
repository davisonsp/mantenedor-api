package cl.coopeuch.challenge.mantenedorapi.enums;

public enum EFrontendMessage {
  HEALTH_MICROSERVICE("0100", "Health", "Mantenedor is alive"),
  INFO_MOCROSERVICE("0100", "Info", "Mantenedor backend project"),
  SUCCESS_REST_SERVICE("0100", "Success", "Success"),
  ERROR_REST_SERVICE("0000", "Error en el sistema", "Se ha producido un error inesperado, intente nuevamente");

  private String code;
  private String title;
  private String description;

  private EFrontendMessage(String code, String title, String description) {
    this.code = code;
    this.title = title;
    this.description = description;
  }

  public String getCode() {
    return code;
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

}
