package cl.coopeuch.challenge.mantenedorapi.enums;

public enum EResponseStatus {
  SUCCESS(1, "success"), WARNING(0, "warning"), FAIL(-1, "fail");

  private final Integer code;
  private final String value;

  private EResponseStatus(Integer code, String value) {
    this.code = code;
    this.value = value;
  }

  public Integer getCode() {
    return code;
  }

  public String getValue() {
    return value;
  }

}
