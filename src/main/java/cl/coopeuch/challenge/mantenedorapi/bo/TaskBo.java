package cl.coopeuch.challenge.mantenedorapi.bo;

import java.util.List;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import cl.coopeuch.challenge.mantenedorapi.dto.Res;
import cl.coopeuch.challenge.mantenedorapi.dto.request.ReqEditTaskDto;
import cl.coopeuch.challenge.mantenedorapi.dto.request.ReqRegisterTaskDto;
import cl.coopeuch.challenge.mantenedorapi.enums.EFrontendMessage;
import cl.coopeuch.challenge.mantenedorapi.repository.model.Task;
import cl.coopeuch.challenge.mantenedorapi.service.ITaskService;

@Component
public class TaskBo {

  private static final Logger LOGGER = LogManager.getLogger(TaskBo.class);

  @Autowired
  private ITaskService taskService;

  public ResponseEntity<Object> getAllTask() {
    LOGGER.info("TaskBo - getTask()");

    List<Task> listTask = taskService.getAllTask();

    if (ObjectUtils.isEmpty(listTask)) {
      return new ResponseEntity<>(new Res(EFrontendMessage.SUCCESS_REST_SERVICE), HttpStatus.OK);
    }

    return new ResponseEntity<>(new Res(EFrontendMessage.SUCCESS_REST_SERVICE, listTask),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> getTask(Long id) {
    LOGGER.info("TaskBo - getTask()");

    Task task = taskService.getTask(id);

    if (ObjectUtils.isEmpty(task)) {
      return new ResponseEntity<>(new Res(EFrontendMessage.ERROR_REST_SERVICE),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity<>(new Res(EFrontendMessage.SUCCESS_REST_SERVICE, task),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> registerTask(ReqRegisterTaskDto reqRegisterTraineeDto) {
    LOGGER.info("TaskBo - registerTask()");

    Task task = new Task(reqRegisterTraineeDto.getDescription());

    taskService.saveTask(task);

    return new ResponseEntity<>(new Res(EFrontendMessage.SUCCESS_REST_SERVICE, task),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> editTask(Long id, ReqEditTaskDto reqEditTaskDto) {
    LOGGER.info("TaskBo - editTask()");

    Task task = taskService.getTask(id);

    if (ObjectUtils.isEmpty(task)) {
      return new ResponseEntity<>(new Res(EFrontendMessage.ERROR_REST_SERVICE),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    task.setDescription(reqEditTaskDto.getDescription());
    task.setActive(reqEditTaskDto.getActive());

    taskService.saveTask(task);

    return new ResponseEntity<>(new Res(EFrontendMessage.SUCCESS_REST_SERVICE, task),
        HttpStatus.OK);
  }

  public ResponseEntity<Object> removeTask(Long id) {
    LOGGER.info("TaskBo - removeTask()");

    Task task = taskService.getTask(id);

    if (ObjectUtils.isEmpty(task)) {
      return new ResponseEntity<>(new Res(EFrontendMessage.ERROR_REST_SERVICE),
          HttpStatus.INTERNAL_SERVER_ERROR);
    }

    task.setStatus('0');

    taskService.saveTask(task);

    return new ResponseEntity<>(new Res(EFrontendMessage.SUCCESS_REST_SERVICE, task),
        HttpStatus.OK);
  }

}
